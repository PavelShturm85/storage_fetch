"""Platform-storage-fetch project entrypoint service file."""
import typing

import fastapi
from fastapi import Depends, exceptions, status
from fastapi.responses import StreamingResponse
from loguru import logger
from platform_jwt_tools import authentication, permissions, roles

from storage_fetch import fetcher, models


ROUTER_OBJ: fastapi.APIRouter = fastapi.APIRouter()


@ROUTER_OBJ.get("/{full_file_path:path}", status_code=status.HTTP_200_OK)
async def fetch_file_from_storage(
    full_file_path: str,
    user: roles.User = authentication.ChatAuthentication(user_permissions=[permissions.IsAuthenticated()]),
    s3_fetcher: fetcher.S3FileFetcher = Depends(fetcher.S3FileFetcher),
) -> typing.Any:
    """Core fetch view."""
    path_parts: typing.List = full_file_path.strip('/').split('/', 1)
    if len(path_parts) != 2:
        raise exceptions.HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail={'error': 'No necessary parts in url'},
        )
    bucket_name: str
    object_key: str
    bucket_name, object_key = path_parts

    try:
        logger.info(f'Try to get file: bucket_name={bucket_name}, object_key={object_key}')
        s3_obj: fetcher.S3Object = await s3_fetcher.build(user=user).fetch_file(bucket_name, object_key)
    except fetcher.UserDoesNotHaveAccess:
        # we're using 404 to completely hide restricted files from user
        raise exceptions.HTTPException(status.HTTP_404_NOT_FOUND, models.BadResponse(error='File not found.').dict())
    except fetcher.FileNotScanned:
        raise exceptions.HTTPException(
            status.HTTP_202_ACCEPTED, models.NotScannedResponse(message='File has not scanned yet.').dict()
        )
    except fetcher.FileScanFailed:
        raise exceptions.HTTPException(
            status.HTTP_403_FORBIDDEN, models.BadResponse(error='Scan has been failed.').dict()
        )
    except fetcher.FileUnknownScanStatus:
        raise exceptions.HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, models.BadResponse(error='Can\'t process file.').dict()
        )
    except fetcher.FileNotFound:
        raise exceptions.HTTPException(status.HTTP_404_NOT_FOUND, models.BadResponse(error='File not found.').dict())

    return StreamingResponse(
        s3_obj.file_generator, media_type=s3_obj.content_type, status_code=fastapi.status.HTTP_200_OK,
    )
