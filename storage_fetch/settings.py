"""Settings file for Platform-storage-fetch project."""
from __future__ import annotations

import envcast


DEBUG: bool = envcast.env('PLATFORM_DEBUG', 'True', type_cast=bool)
API_PREFIX: str = envcast.env('STORAGE_FETCH_PUBLIC_API_PREFIX', '/storage-fetch/').rstrip('/')
DEFAULT_TRIES: str = envcast.env('PLATFORM_DEFAULT_TRIES', 3, type_cast=int)
DOC_PREFIX: str = envcast.env('STORAGE_FETCH_DOC_PREFIX', '/doc/api/storage-fetch/').rstrip('/') + '/'
S3_ENDPOINT: str = envcast.env('PLATFORM_S3_ENDPOINT', 'http://127.0.0.1:9060', type_cast=str)
S3_ACCESS_KEY: str = envcast.env('PLATFORM_S3_ACCESS_KEY', 'minioadmin', type_cast=str)
S3_SECRET_KEY: str = envcast.env('PLATFORM_S3_SECRET_KEY', 'minioadmin', type_cast=str)
S3_BUCKET: str = envcast.env('PLATFORM_S3_BUCKET', 'test', type_cast=str)
S3_CHUNK_SIZE: int = envcast.env('STORAGE_FETCH_S3_CHUNK_SIZE', 70 * 1024, type_cast=int)
JWT_PUBLIC_KEY: str = envcast.env('PLATFORM_JWT_PUBLIC_KEY', '', type_cast=str,)

SENTRY_DSN: str = envcast.env(
    'STORAGE_FETCH_SENTRY_DSN', '', type_cast=str,
)
SCAN_STATUS_KEY: str = envcast.env(
    'PLATFORM_SCAN_STATUS_KEY', 'scan_status', type_cast=str,
)
NOT_SCANNED_STATUS: str = envcast.env(
    'PLATFORM_NOT_SCANNED_STATUS', 'not_scanned', type_cast=str,
)
SCAN_FAILED_STATUS: str = envcast.env('PLATFORM_SCAN_FAILED_STATUS', 'scan_failed', type_cast=str)
SCAN_DONE_STATUS: str = envcast.env('PLATFORM_SCAN_DONE_STATUS', 'scan_done', type_cast=str)

UVICORN: dict = {
    'APP_PORT': envcast.env('STORAGE_FETCH_APP_PORT', 9991, type_cast=int),
    'LOG_LEVEL': envcast.env('PLATFORM_LOG_LEVEL', 'info', type_cast=str),
    'APP_WORKERS': envcast.env('STORAGE_FETCH_APP_WORKERS', 3, type_cast=int),
    'APP_LIMIT_MAX_REQUESTS': envcast.env('STORAGE_FETCH_APP_LIMIT_MAX_REQUESTS', 3000, type_cast=int),
    'APP_LOOP': envcast.env('STORAGE_FETCH_APP_LOOP', 'uvloop', type_cast=str),
}
