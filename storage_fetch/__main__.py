"""Platform-storage-fetch project entrypoint service file."""
import fastapi
import sentry_sdk
from chat_log_tools import middleware as log_middleware
from chat_log_tools import setup_json_logger
from fastapi import status
from health_checks.api.fastapi import get_health_check_router
from platform_jwt_tools import config, security
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
from starlette.middleware.cors import CORSMiddleware
from uvicorn import Config, Server

from storage_fetch import connections, error_handlers, settings, views


async def startup():
    """Global initialization."""
    await connections.S3ClientConnectionSingleton().initialize()


async def shutdown():
    """Global shutdown."""
    await connections.S3ClientConnectionSingleton().shutdown()


if settings.SENTRY_DSN:
    sentry_sdk.init(dsn=settings.SENTRY_DSN)

APP_OBJ: fastapi.FastAPI = fastapi.FastAPI(
    docs_url=settings.DOC_PREFIX,
    on_startup=[startup],
    on_shutdown=[shutdown],
    exception_handlers={status.HTTP_500_INTERNAL_SERVER_ERROR: error_handlers.handle_500_exception},
    openapi_url=f"{settings.API_PREFIX}/openapi.json",
)
APP_OBJ.include_router(views.ROUTER_OBJ, prefix=settings.API_PREFIX)
APP_OBJ.include_router(get_health_check_router(), prefix=settings.API_PREFIX)
APP_OBJ.add_middleware(log_middleware.RequestIDASGIMiddleware)
APP_OBJ.add_middleware(SentryAsgiMiddleware)

if settings.DEBUG:
    APP_OBJ.add_middleware(
        CORSMiddleware, allow_origins=["*"], allow_credentials=True, allow_methods=["*"], allow_headers=["*"],
    )


if __name__ == '__main__':
    config.set_config(config.Config(verify_exp=False))
    uvicorn_server: Server = Server(
        Config(
            APP_OBJ,
            host='0.0.0.0',
            port=settings.UVICORN['APP_PORT'],
            log_level=settings.UVICORN['LOG_LEVEL'],
            workers=settings.UVICORN['APP_WORKERS'],
            loop=settings.UVICORN['APP_LOOP'],
            limit_max_requests=settings.UVICORN['APP_LIMIT_MAX_REQUESTS'],
        )
    )
    setup_json_logger(need_sentry=bool(settings.SENTRY_DSN))
    security.set_jwt_public_key(settings.JWT_PUBLIC_KEY)
    uvicorn_server.run()
