"""Helpers."""
import typing
from dataclasses import dataclass

import botocore.exceptions
from fastapi import Depends
from loguru import logger
from platform_jwt_tools import roles

from storage_fetch import connections, settings


@dataclass(frozen=True)
class S3Object:
    """Dataclass with s3 file data."""

    file_generator: typing.AsyncGenerator
    content_type: str


class FileNotScanned(Exception):
    """File not scanned error."""


class FileScanFailed(Exception):
    """File scan failed error."""


class FileUnknownScanStatus(Exception):
    """File unknown scan status error."""


class FileNotFound(Exception):
    """File not found error."""


class UserDoesNotHaveAccess(Exception):
    """User does not have access error."""


class S3FileFetcher:
    """Class for fetching file from s3."""

    def __init__(self, s3_client=Depends(connections.S3ClientConnectionSingleton)):
        self._s3_client: typing.Any = s3_client.client
        self._user: typing.Optional[roles.User] = None

    def build(self, user: roles.User):
        """Inject user."""
        self._user = user
        return self

    def _is_404_exc(self, s3_client_exc: botocore.exceptions.ClientError):
        """Check if botocore client exc has 404 code."""
        return bool(s3_client_exc.response.get('Error', {}).get('Code', '0') == '404')

    async def _validate_metadata(self, bucket_name: str, object_key: str) -> None:
        """Validate file metadata."""
        try:
            head_response: typing.Mapping = await self._s3_client.head_object(Bucket=bucket_name, Key=object_key)
            object_metadata: typing.Mapping = head_response['Metadata']
            user_id_in_metadata: str = object_metadata.get('user_id', '')
            if roles.is_public_user(self._user) and self._user.user_info.user_id != user_id_in_metadata:  # type: ignore
                logger.error(
                    f'User {self._user.user_info.user_id} tries to access '  # type: ignore
                    f'object with user_id={user_id_in_metadata} in metadata'
                )
                raise UserDoesNotHaveAccess()

            scan_status: str = object_metadata.get(settings.SCAN_STATUS_KEY, '')
            if scan_status == settings.NOT_SCANNED_STATUS:
                raise FileNotScanned()
            if scan_status == settings.SCAN_FAILED_STATUS:
                raise FileScanFailed()
            if scan_status != settings.SCAN_DONE_STATUS:
                logger.error(f'Unknown scan status in file metadata: {scan_status}!')
                raise FileUnknownScanStatus()
        except botocore.exceptions.ClientError as exc:
            if self._is_404_exc(exc):
                raise FileNotFound()
            # exception will be caught by the handler
            raise

    async def _fetch_and_wait_for_file(self, bucket_name: str, object_key: str) -> S3Object:
        """Wait for file."""
        try:
            s3_obj = await self._s3_client.get_object(Bucket=bucket_name, Key=object_key)
        except botocore.exceptions.ClientError as exc:
            if self._is_404_exc(exc):
                logger.warning('Weird: `head_object` returns metadata, but `get_object` throws 404')
                raise FileNotFound()
            raise

        return S3Object(
            file_generator=s3_obj['Body'].iter_chunks(chunk_size=settings.S3_CHUNK_SIZE),
            content_type=s3_obj['ResponseMetadata']['HTTPHeaders']['content-type'],
        )

    async def fetch_file(self, bucket_name: str, object_key: str) -> S3Object:
        """Fetch file from s3."""
        if not self._user:
            raise RuntimeError('You should call `build` first!')
        await self._validate_metadata(bucket_name, object_key)
        return await self._fetch_and_wait_for_file(bucket_name, object_key)
