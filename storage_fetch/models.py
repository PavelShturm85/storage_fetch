"""Pydantic models here."""
from __future__ import annotations
import typing

from pydantic import BaseModel


class BadResponse(BaseModel):
    """Generic bad response."""

    error: str


class BadFileResponse(BadResponse):
    """Bad response for file."""

    filename: str


class OneUpload(BaseModel):
    """Model for one upload."""

    url: str
    file_size: typing.Optional[int] = None
    file_name: typing.Optional[str] = None


class GoodResponse(BaseModel):
    """Good response."""

    files: typing.List[OneUpload]


class NotScannedResponse(BaseModel):
    """Not scanned response."""

    message: str
