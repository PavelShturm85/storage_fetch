"""Connections lies here."""
import contextlib
import typing

import aioboto3
import aiohttp.client_exceptions
import backoff

from storage_fetch import settings


class S3ClientConnectionSingleton:
    """Singleton for s3 client."""

    client: typing.Any = None
    _context_stack: typing.Optional[contextlib.AsyncExitStack] = None

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super().__new__(cls)
        return cls.instance

    @backoff.on_exception(
        backoff.expo, aiohttp.client_exceptions.ClientConnectorError, max_tries=settings.DEFAULT_TRIES,
    )
    async def initialize(self):
        """Initialize s3 client."""
        self._context_stack = contextlib.AsyncExitStack()
        self.client = await self._context_stack.enter_async_context(
            aioboto3.client(
                's3',
                endpoint_url=settings.S3_ENDPOINT,
                aws_access_key_id=settings.S3_ACCESS_KEY,
                aws_secret_access_key=settings.S3_SECRET_KEY,
            )
        )

    async def shutdown(self) -> None:
        """Close s3 client."""
        if self._context_stack:
            await self._context_stack.aclose()
