Platform-storage-fetch
===
This service fetches and validates files from s3.


## CLI
1. dev scripts (located in [./bin/scripts](./bin/scripts))
    * `./bin/scripts/build-docker.sh` — helper for docker building
    * `./bin/scripts/run.sh` — dev server
    * `/.bin/scripts/run-lint.sh` - run linting
1. production scripts (located in [./bin/app](./bin/app))
    * `./bin/app/run.sh` — main application server


## Quickstart
* local development
    * just run `./bin/scripts/run.sh`


## Linting
* disabled `bad-continuation` in `.pylintrc` because of incompatibility with pylint and black
* disabled `raise-missing-from` in `.pylintrc` because http exceptions not the real ones
* disabled `no-self-use` because we don't use static methods


## Service API
1. check on url `/doc/api/storage-fetch/`


## ENV variables
Default:
* `PLATFORM_DEBUG` bool
* `PLATFORM_PUBLIC_API_PREFIX` default is `/api/upload/`
* `STORAGE_FETCH_DOC_PREFIX` default is `/doc/api/service-upload/`
* `STORAGE_FETCH_S3_CHUNK_SIZE` default is 70 * 1024

Necessary:
* `PLATFORM_S3_BUCKET`
* `PLATFORM_S3_WEB_SERVER_VISIBLE_URL`
* `PLATFORM_S3_ENDPOINT`
* `PLATFORM_S3_ACCESS_KEY`
* `PLATFORM_S3_SECRET_KEY`
