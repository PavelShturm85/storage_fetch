"""Basic fixtures lies here."""
import fastapi
import pytest
from chat_log_tools import middleware as log_middleware
from chat_log_tools import setup_json_logger
from fastapi import status

from storage_fetch import error_handlers, settings, views


@pytest.fixture(scope='module')
def app():
    """App fixture."""
    setup_json_logger(need_sentry=False)
    app_obj: fastapi.FastAPI = fastapi.FastAPI(
        docs_url=settings.DOC_PREFIX,
        exception_handlers={status.HTTP_500_INTERNAL_SERVER_ERROR: error_handlers.handle_500_exception},
    )
    app_obj.include_router(views.ROUTER_OBJ, prefix=settings.API_PREFIX)
    app_obj.add_middleware(log_middleware.RequestIDASGIMiddleware)
    return app_obj
