"""Tests for fetcher."""
import pytest
from hypothesis import given, strategies

from storage_fetch import fetcher
from tests import mock


@given(strategies.binary())
@pytest.mark.asyncio
async def test_fetcher_without_build(file_data: bytes):
    """Test fetcher without `build` call."""
    fetcher_obj = fetcher.S3FileFetcher(
        mock.S3ClientConnectionSingletonMock(
            Bucket='kek', Key='kek.jpg', metadata={}, file_data=file_data, content_type='image/jpeg',
        )
    )

    with pytest.raises(RuntimeError):
        await fetcher_obj.fetch_file('kek', 'kek.jpg')
