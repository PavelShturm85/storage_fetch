"""Dirty application tests with monkeypatching etc."""
from unittest import mock

import pytest


def test_app_creating(monkeypatch):
    """Test app creating."""
    monkeypatch.setattr('storage_fetch.settings.SENTRY_DSN', 'https://this-is-pure-fake@fake-for-test/6')
    # pylint: disable=import-outside-toplevel
    from storage_fetch import __main__

    assert __main__.APP_OBJ


@pytest.mark.asyncio
async def test_startup(monkeypatch):
    """Test for startup."""
    monkeypatch.setattr('aioboto3.client', mock.MagicMock())
    # pylint: disable=import-outside-toplevel
    from storage_fetch import connections
    from storage_fetch.__main__ import startup

    await startup()

    assert connections.S3ClientConnectionSingleton().client


@pytest.mark.asyncio
async def test_shutdown(monkeypatch):
    """Test for shutdown."""
    monkeypatch.setattr('aioboto3.client', mock.AsyncMock())
    # pylint: disable=import-outside-toplevel
    from storage_fetch.__main__ import shutdown

    await shutdown()
