"""Mocks lies here."""


class FileDataMock:
    """Mock for file data."""

    def __init__(self, file_data):
        self._file_data = file_data

    # pylint: disable=unused-argument
    async def iter_chunks(self, chunk_size=0):
        """Iter chunks mock."""
        yield self._file_data


class S3ClientMock:
    """Mock for s3 client."""

    # pylint: disable=too-many-arguments
    def __init__(self, metadata, file_data, content_type, bucket, key, head_exception=None, get_exception=None):
        self._metadata = metadata
        self._file_data = file_data
        self._content_type = content_type
        self._bucket = bucket
        self._key = key
        self._head_exception = head_exception
        self._get_exception = get_exception

    # pylint: disable=invalid-name
    async def head_object(self, Bucket, Key):
        """Head object mock."""
        if self._head_exception:
            raise self._head_exception
        if Bucket == self._bucket and Key == self._key:
            return {'Metadata': self._metadata}

    # pylint: disable=invalid-name
    async def get_object(self, Bucket, Key):
        """Get object mock."""
        if self._get_exception:
            raise self._get_exception
        if Bucket == self._bucket and Key == self._key:
            return {
                'Metadata': self._metadata,
                'Body': FileDataMock(file_data=self._file_data),
                'ResponseMetadata': {'HTTPHeaders': {'content-type': self._content_type}},
            }


class S3ClientConnectionSingletonMock:
    """Mock for s3 client connection singleton."""

    # pylint: disable=too-many-arguments
    def __init__(
        self, metadata=None, file_data=b'', content_type='', Bucket='', Key='', head_exception=None, get_exception=None
    ):
        self.client = S3ClientMock(metadata, file_data, content_type, Bucket, Key, head_exception, get_exception)
