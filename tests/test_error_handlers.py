"""Tests for error handlers."""
import json
from unittest.mock import Mock

import pytest
from fastapi import HTTPException
from fastapi.responses import JSONResponse

from storage_fetch import error_handlers


@pytest.mark.asyncio
async def test_json_on_unknown_error():
    """Test json response on unknown error."""
    result = await error_handlers.handle_500_exception(Mock(), Exception())

    assert isinstance(result, JSONResponse)
    assert json.loads(result.body.decode('utf-8'))['detail']


@pytest.mark.asyncio
async def test_reraise_json_http_exception():
    """Test re-raise json http exception."""
    with pytest.raises(HTTPException):
        await error_handlers.handle_500_exception(Mock(), HTTPException(status_code=400, detail={'kek': 'lol'}))
