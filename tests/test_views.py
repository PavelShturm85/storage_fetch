"""Tests for views."""
import botocore.exceptions
import hypothesis
import pytest
from fastapi.testclient import TestClient
from platform_jwt_tools.tests.dataclasses import ClientTestData
from requests.models import Response

from storage_fetch import connections, settings
from tests import mock


PNG_MIME: str = 'image/png'
PNG_FILE_NAME: str = 'kek.png'
JPG_MIME: str = 'image/jpeg'
JPG_FILE_NAME: str = 'kek.jpg'


@hypothesis.settings(suppress_health_check=(hypothesis.HealthCheck.function_scoped_fixture,))
@pytest.mark.parametrize(
    'content_type, extension', ((PNG_MIME, '.txt'), (JPG_MIME, '.jpg'), ('image/gif', '.gif'), ('text/plain', '.txt')),
)
@hypothesis.given(
    hypothesis.strategies.binary(),
    hypothesis.strategies.text(alphabet=hypothesis.strategies.characters(whitelist_categories=['Lu', 'Ll', 'Nd'])),
)
def test_fetch_scanned_file(
    public_fastapi_test_client: ClientTestData, content_type: str, extension: str, file_data: bytes, file_name: str
):
    """Test fetch scanned file."""
    fastapi_client: TestClient = public_fastapi_test_client.fastapi_client
    file_name_with_extension: str = f'{file_name}{extension}'
    fastapi_client.app.dependency_overrides[  # type: ignore
        connections.S3ClientConnectionSingleton
    ] = lambda: mock.S3ClientConnectionSingletonMock(
        metadata={
            'user_id': public_fastapi_test_client.user_jwt_data.user.user_info.user_id,
            'scan_status': settings.SCAN_DONE_STATUS,
        },
        content_type=content_type,
        file_data=file_data,
        Bucket='kek',
        Key=file_name_with_extension,
    )

    result: Response = fastapi_client.get(f'{settings.API_PREFIX}/kek/{file_name_with_extension}')

    assert result.status_code == 200


def test_fetch_with_bad_url(public_fastapi_test_client: ClientTestData):
    """Test bad response for bad --dragon-- url."""
    result: Response = public_fastapi_test_client.fastapi_client.get(f'{settings.API_PREFIX}/kek')

    assert result.status_code == 400


@hypothesis.settings(suppress_health_check=(hypothesis.HealthCheck.function_scoped_fixture,))
@pytest.mark.parametrize(
    'head_object_404, get_object_404', ((True, False), (False, True)),
)
@hypothesis.given(hypothesis.strategies.binary())
def test_fetch_not_existing_file(
    public_fastapi_test_client: ClientTestData, head_object_404: bool, get_object_404: bool, file_data: bytes
):
    """Test 404 for not existing file."""
    fastapi_client: TestClient = public_fastapi_test_client.fastapi_client
    file_name: str = JPG_FILE_NAME
    fastapi_client.app.dependency_overrides[  # type: ignore
        connections.S3ClientConnectionSingleton
    ] = lambda: mock.S3ClientConnectionSingletonMock(
        metadata={
            'user_id': public_fastapi_test_client.user_jwt_data.user.user_info.user_id,
            'scan_status': settings.SCAN_DONE_STATUS,
        },
        content_type=JPG_MIME,
        file_data=file_data,
        Bucket='kek',
        Key=JPG_FILE_NAME,
        head_exception=botocore.exceptions.ClientError({'Error': {'Code': '404'}}, 'test') if head_object_404 else None,
        get_exception=botocore.exceptions.ClientError({'Error': {'Code': '404'}}, 'test') if get_object_404 else None,
    )

    result: Response = fastapi_client.get(f'{settings.API_PREFIX}/kek/{file_name}')

    assert result.status_code == 404


@hypothesis.settings(suppress_health_check=(hypothesis.HealthCheck.function_scoped_fixture,))
@pytest.mark.parametrize(
    'head_object_unknown, get_object_unknown', ((True, False), (False, True)),
)
@hypothesis.given(hypothesis.strategies.binary())
def test_unknown_s3_exception(
    public_fastapi_test_client: ClientTestData, head_object_unknown: bool, get_object_unknown: bool, file_data: bytes,
):
    """Test 500 for unknown s3 exception."""
    fastapi_client: TestClient = public_fastapi_test_client.fastapi_client
    fastapi_client.app.dependency_overrides[  # type: ignore
        connections.S3ClientConnectionSingleton
    ] = lambda: mock.S3ClientConnectionSingletonMock(
        metadata={
            'user_id': public_fastapi_test_client.user_jwt_data.user.user_info.user_id,
            'scan_status': settings.SCAN_DONE_STATUS,
        },
        content_type=JPG_MIME,
        file_data=file_data,
        Bucket='kek',
        Key=JPG_FILE_NAME,
        head_exception=botocore.exceptions.ClientError({'Code': '500'}, 'test') if head_object_unknown else None,
        get_exception=botocore.exceptions.ClientError({'Code': '500'}, 'test') if get_object_unknown else None,
    )

    result: Response = fastapi_client.get(f'{settings.API_PREFIX}/kek/{JPG_FILE_NAME}')

    assert result.status_code == 500
    assert result.json()['detail']


@hypothesis.settings(suppress_health_check=(hypothesis.HealthCheck.function_scoped_fixture,))
@hypothesis.given(hypothesis.strategies.binary(), hypothesis.strategies.uuids())
def test_restricted_access(public_fastapi_test_client: ClientTestData, file_data: bytes, user_id: str):
    """Test 404 when user tries to access not his file."""
    fastapi_client: TestClient = public_fastapi_test_client.fastapi_client
    fastapi_client.app.dependency_overrides[  # type: ignore
        connections.S3ClientConnectionSingleton
    ] = lambda: mock.S3ClientConnectionSingletonMock(
        metadata={'user_id': user_id, 'scan_status': settings.SCAN_DONE_STATUS,},
        content_type=PNG_MIME,
        file_data=file_data,
        Bucket='kek',
        Key=PNG_FILE_NAME,
    )

    result: Response = fastapi_client.get(f'{settings.API_PREFIX}/kek/{PNG_FILE_NAME}')

    assert result.status_code == 404


@hypothesis.settings(suppress_health_check=(hypothesis.HealthCheck.function_scoped_fixture,))
@hypothesis.given(hypothesis.strategies.binary())
def test_not_scanned_file(public_fastapi_test_client: ClientTestData, file_data: bytes):
    """Test 202 for not scanned file."""
    fastapi_client: TestClient = public_fastapi_test_client.fastapi_client
    fastapi_client.app.dependency_overrides[  # type: ignore
        connections.S3ClientConnectionSingleton
    ] = lambda: mock.S3ClientConnectionSingletonMock(
        metadata={
            'user_id': public_fastapi_test_client.user_jwt_data.user.user_info.user_id,
            'scan_status': settings.NOT_SCANNED_STATUS,
        },
        content_type=PNG_MIME,
        file_data=file_data,
        Bucket='kek',
        Key=PNG_FILE_NAME,
    )

    result: Response = fastapi_client.get(f'{settings.API_PREFIX}/kek/{PNG_FILE_NAME}')

    assert result.status_code == 202


@hypothesis.settings(suppress_health_check=(hypothesis.HealthCheck.function_scoped_fixture,))
@hypothesis.given(hypothesis.strategies.binary())
def test_scan_failed_file(public_fastapi_test_client: ClientTestData, file_data: bytes):
    """Test 403 for scan failed file."""
    fastapi_client: TestClient = public_fastapi_test_client.fastapi_client
    fastapi_client.app.dependency_overrides[  # type: ignore
        connections.S3ClientConnectionSingleton
    ] = lambda: mock.S3ClientConnectionSingletonMock(
        metadata={
            'user_id': public_fastapi_test_client.user_jwt_data.user.user_info.user_id,
            'scan_status': settings.SCAN_FAILED_STATUS,
        },
        content_type=PNG_MIME,
        file_data=file_data,
        Bucket='kek',
        Key=PNG_FILE_NAME,
    )

    result: Response = fastapi_client.get(f'{settings.API_PREFIX}/kek/{PNG_FILE_NAME}')

    assert result.status_code == 403


@hypothesis.settings(suppress_health_check=(hypothesis.HealthCheck.function_scoped_fixture,))
@hypothesis.given(hypothesis.strategies.binary())
def test_bad_scan_status(public_fastapi_test_client: ClientTestData, file_data: bytes):
    """Test 500 for bad scan status."""
    fastapi_client: TestClient = public_fastapi_test_client.fastapi_client
    fastapi_client.app.dependency_overrides[  # type: ignore
        connections.S3ClientConnectionSingleton
    ] = lambda: mock.S3ClientConnectionSingletonMock(
        metadata={'user_id': public_fastapi_test_client.user_jwt_data.user.user_info.user_id, 'scan_status': 'kek',},
        content_type=PNG_MIME,
        file_data=file_data,
        Bucket='kek',
        Key=PNG_FILE_NAME,
    )

    result: Response = fastapi_client.get(f'{settings.API_PREFIX}/kek/{PNG_FILE_NAME}')

    assert result.status_code == 500
