#!/usr/bin/env bash

uvicorn storage_fetch.__main__:APP_OBJ\
    --reload\
    --reload-dir storage_fetch\
    --host 0.0.0.0\
    --port ${STORAGE_FETCH_APP_PORT:-8052}\
    --log-level debug
